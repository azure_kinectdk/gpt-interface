import svelte from 'rollup-plugin-svelte' ;
import resolve from '@rollup/plugin-node-resolve' ;

export default {
  input: 'src/main.js',
  output: {
    file: 'public/build/main.js',
    format: 'iife'
  },
  plugins: [
    svelte({

      // Extract CSS into a separate file (recommended).
      // See note below
      css: function (css) {
        css.write('public/build/main.css');
      },

      // Warnings are normally passed straight to Rollup. You can
      // optionally handle them here, for example to squelch
      // warnings with a particular code
      onwarn: (warning, handler) => {
        // e.g. don't warn on <marquee> elements, cos they're cool
        // if (warning.code === 'a11y-distracting-elements') return;

        // let Rollup handle all other warnings normally
        handler(warning);
      }
    }),
    resolve() ,
  ]
}